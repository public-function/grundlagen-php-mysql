<?php

use PHPMailer\PHPMailer\PHPMailer;

require_once __DIR__.'/../lib/PHPMailer-6.1.7/src/Exception.php';
require_once __DIR__.'/../lib/PHPMailer-6.1.7/src/PHPMailer.php';
require_once __DIR__.'/../lib/PHPMailer-6.1.7/src/SMTP.php';


class Mail {

	/**
	 * @return PHPMailer
	 */
	private function buildMailer() {
		$mailer = new PHPMailer();
		$mailer->Host = "";
		$mailer->SMTPAuth = true;
		$mailer->Username = '';
		$mailer->Password = '';
		$mailer->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
		$mailer->Port = 587;
		return $mailer;
	}

	/**
	 * @param string $subject
	 * @param string $message
	 * @param string $to
	 * @return bool
	 * @throws \PHPMailer\PHPMailer\Exception
	 */
	public function send($subject, $message, $to){
		$mailer = $this->buildMailer();
		$mailer->setFrom("no-reply@edwardbock.de");
		$mailer->addAddress($to);
		$mailer->Subject = $subject;
		$mailer->Body = $message;
		return $mailer->send();
	}
}