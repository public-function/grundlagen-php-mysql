<?php


/**
 * @property Database database
 * @property Template template
 */
class Application {
	public function __construct() {
		$this->database = new Database();
		$this->template = new Template();
	}
}