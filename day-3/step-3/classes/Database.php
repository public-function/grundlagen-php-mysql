<?php

/**
 * Class Database
 */
class Database{

	/**
	 * @var mysqli
	 */
	private $connection;

	/**
	 * Database constructor.
	 */
	public function __construct() {
		$this->connection = new mysqli("localhost", "butler", "butler", "butler");
	}

	/**
	 * @param string $matriculation
	 * @param string $name
	 * @return int|false
	 */
	function addStudent($matriculation, $name){
		$name = $this->connection->real_escape_string($name);
		$matriculation = $this->connection->real_escape_string($matriculation);
		$success = $this->connection->query("
				INSERT INTO students (`name`, matriculation) VALUES ('$name', '$matriculation')
		");
		if(!$success) return false;
		return $this->connection->insert_id;
    }
    
    /**
     * @return Student[]
     */
    function getStudents(){
        $result = $this->connection->query("SELECT * FROM students");
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        if(!is_array($rows)) return [];
        return array_map("mapRowToStudent", $rows);
	}

	/**
	 * @param string $query
	 * @return Student[]
	 */
	function findStudents($query){
		$query = $this->connection->real_escape_string($query);
		$result = $this->connection->query("SELECT * FROM students WHERE `name` LIKE '%$query%' ORDER BY `name` ASC");
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		if(!is_array($rows)) return [];
		return array_map("mapRowToStudent", $rows);
	}
	
	/**
	 * @return Course[]
	 */
	function getCourses(){
		$result = $this->connection->query("SELECT * FROM courses");
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        if(!is_array($rows)) return [];
		return array_map("mapRowToCourse", $rows);
	}

	/**
	 * @param int $student_id
	 * @param int $course_id
	 * @return boolean
	 */
	function isInCourse($student_id, $course_id){
		$student_id = intval($student_id);
		$course_id = intval($course_id);
		$result = $this->connection->query("SELECT count(*) as `count` FROM students_in_courses WHERE student_id = $student_id AND course_id = $course_id");
		$field = intval($result->fetch_object()->count);
		return $field > 0;
	}

	/**
	 * @param int $studentId
	 * @param int $courseId
	 * @return bool
	 */
	function takeCourse($studentId, $courseId){
		$studentId = intval($studentId);
		$courseId = intval($courseId);
		return $this->connection->query("INSERT INTO students_in_courses (student_id, course_id) VALUES ($studentId, $courseId)");
	}

	/**
	 * @param int $studentId
	 * @param int $courseId
	 * @return bool
	 */
	function exitCourse($studentId, $courseId){
		$studentId = intval($studentId);
		$courseId = intval($courseId);
		$query = "DELETE FROM students_in_courses WHERE course_id = $courseId AND student_id = $studentId";
		return $this->connection->query($query);
	}
	
	/**
	 * @param int $course_id
	 * @return Student[]
	 */
	function getStudentsInCourse($course_id){
		$course_id = intval($course_id);
		$result = $this->connection->query(
            "SELECT students.id as id, `name`, matriculation FROM students_in_courses as sic ".
            "LEFT JOIN students ON (sic.student_id = students.id)".
            "WHERE course_id = $course_id;"
        );
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        if(!is_array($rows)) return [];
        return array_map("mapRowToStudent",$rows);
	}
    
    /**
     * @param int $studentId
     * @return Course[]
     */
    function getCoursesForStudent($studentId){
        $studentId = intval($studentId);
        $result = $this->connection->query("
			SELECT courses.id as id, title FROM students_in_courses as sic 
            LEFT JOIN courses ON (sic.course_id = courses.id)
            WHERE student_id = $studentId;
            ");
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        if(!is_array($rows)) return [];
        return array_map("mapRowToCourse", $rows);
    }

	/**
	 * @param int $studentId
	 * @param int $courseId
	 * @param int $grade
	 *
	 * @return boolean
	 */
	function setGrade($studentId, $courseId, $grade){
		// if connection gets accessed from outside this is a "side effect"
		$studentId = intval($studentId);
		$courseId = intval($courseId);
		$grade = intval($grade);
		return $this->connection->query(
			"UPDATE students_in_courses SET grade = $grade WHERE student_id = $studentId AND course_id = $courseId"
		);
	}

}