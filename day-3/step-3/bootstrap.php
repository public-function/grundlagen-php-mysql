<?php

require_once __DIR__."/utils/mapper.php";

require_once __DIR__."/classes/Student.php";
require_once __DIR__."/classes/Course.php";
require_once __DIR__."/classes/Database.php";

require_once __DIR__."/classes/Template.php";
require_once __DIR__."/classes/Mail.php";
require_once __DIR__."/classes/Application.php";

