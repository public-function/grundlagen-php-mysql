<?php

require_once dirname(__FILE__)."/bootstrap.php";

$app = new Application();
// ---------------------------------------------------
// check if course was selected
// ---------------------------------------------------
$selectedCourseId = false;
if(isset($_GET["course_id"])){
	$selectedCourseId = intval($_GET["course_id"]);
}

// ---------------------------------------------
// add student to course if form was submitted
// ---------------------------------------------
if(
	isset($_POST["add_student_id"]) && !empty($_POST["add_student_id"])
    &&
	isset($_POST["add_course_id"]) && !empty($_POST["add_course_id"])
){
	$studentId = intval($_POST["add_student_id"]);
	$courseId = intval($_POST["add_course_id"]);
	$app->database->takeCourse($studentId, $courseId);
}

// ---------------------------------------------------
// remove student from course if form was submitted
// ---------------------------------------------------
if(
	isset($_POST["remove_student_id"]) && !empty($_POST["remove_student_id"])
    &&
	isset($_POST["remove_course_id"]) && !empty($_POST["remove_course_id"])
){
	$studentId = intval($_POST["remove_student_id"]);
	$courseId = intval($_POST["remove_course_id"]);
	$app->database->exitCourse($studentId, $courseId);
}

$app->template->renderHead("Kurse");
?>

<p><a href="index.php">Zurück zur Übersicht</a></p>

<div style="padding: 20px">
        <form method="GET">
            <label>
                Kurs<br/>
                <select name="course_id">
                    <option value="">-- Kurs auswählen --</option>
                    <?php
                    // ----------------------------------------------------------
                    // render all existing courses and select if one is selected
                    // ----------------------------------------------------------
                    $courses = $app->database->getCourses();
                    foreach($courses as $course){
                        $selected = "";
                        if($selectedCourseId == $course->id){
                            $selected = "selected";
                        }
                        echo "<option $selected value='$course->id'>$course->title</option>";
                    }
                    ?>
                </select>
            </label>
            <button>Auswählen</button>
        </form>
</div>

<?php
// ---------------------------------------------------
// render students list if course is selected
// ---------------------------------------------------
if(false != $selectedCourseId){

    ?>
    <div style="padding: 20px;" class="row">
        <div class="col-md-6">
            <form method="POST">
                <input type="hidden" value="<?php echo $selectedCourseId; ?>" name="add_course_id" />
                <label>
                    Student<br/>
                    <select name="add_student_id">
                        <?php
                        // ---------------------------------------------------
                        // render all students that are not in selected course
                        // ---------------------------------------------------
                        $students = $app->database->getStudents();
                        foreach($students as $student){

                            $isInCourse = $app->database->isInCourse($student->id, $selectedCourseId);
                            if($isInCourse) continue;

                            echo "<option value='$student->id'>$student->name</option>";
                        }
                        ?>
                    </select>
                </label>
                <button>Hinzufügen</button>
            </form>
        </div>
        <div class="col-md-6">
            <?php
            // TODO: import students list from csv
            ?>
            <form method="post" enctype="multipart/form-data">
                <label>
                    Studenten importieren<br/>
                    <input type="file" name="import_students" />
                </label>
                <button>Importieren</button>
                <p>Die CSV muss aus zwei spalten bestehen. Erste Spalte Matrikelnummer, zweite Spalte Name des Studenten.<br/>
                Beispiel:<br/>
                <code>123,Max Mustermann<br/>
                    321,Martina Musterfrau
                </code></p>
            </form>
        </div>
    </div>

    <?php

	// ---------------------------------------------------
	// render students that are participating in course
	// ---------------------------------------------------
    $students = $app->database->getStudentsInCourse($selectedCourseId);

    echo "<table class='table'>";
    echo "<tr><th style='width: 160px'>Matrikelnummer</th><th>Name</th><th>Action</th></tr>";
    foreach($students as $student){
        echo "<tr>";
        echo "<td>$student->matriculation</td><td>$student->name</td>";
        ?>
        <td>
            <form method="post">
                <input type="hidden" value="<?php echo $selectedCourseId; ?>" name="remove_course_id" />
                <input type="hidden" value="<?php echo $student->id ?>" name="remove_student_id" />
                <button>Aus Kurs entfernen</button>
            </form>
        </td>
        <?php
        echo "</tr>";
    }
    echo "</table>";
} else {
    echo "<p>Wähle einen Kurs, dann werden alle Studenten in diesem Kurs aufgelistet.";
}

$app->template->renderFoot();
?>


