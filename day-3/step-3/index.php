<?php

// --------------------------------------
// setup application
// --------------------------------------
require_once __DIR__."/bootstrap.php";
$app = new Application();

// --------------------------------------
// handle insert single student request
// --------------------------------------
$newStudentId = false;
if(
	isset($_POST["insert_student_name"]) && !empty($_POST["insert_student_name"])
	&&
	isset($_POST["insert_student_matrikel"]) && !empty($_POST["insert_student_matrikel"])
){
	$newStudentId = $db->addStudent( $_POST["insert_student_matrikel"] , $_POST["insert_student_name"] );
}

$app->template->renderHead("Übersicht");

?>
	<h1>Verwaltung</h1>
	<p><a href="students.php">Studenten verwalten</a></p>
	<p><a href="courses.php">Kurse verwalten</a></p>

<?php
$app->template->renderFoot();
?>
