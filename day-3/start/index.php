<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>HTML with PHP Template</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet"
	      href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
	      integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous"
	/>
</head>
<body style="padding: 10px">
	<h1>Verwaltung</h1>
	<p><a href="students.php">Studenten verwalten</a></p>
	<p><a href="courses.php">Kurse verwalten</a></p>
</body>
</html>

