
DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
);

DROP TABLE IF EXISTS `grades`;
CREATE TABLE `grades` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `grade` varchar(10) NOT NULL,
  `grade_value` double NOT NULL,
  `grade_definition` text DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `grade` (`grade`),
  UNIQUE KEY `grade_value` (`grade_value`)
);

DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `matriculation` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matriculation` (`matriculation`) USING BTREE,
  KEY `name` (`name`)
);

DROP TABLE IF EXISTS `students_in_courses`;
CREATE TABLE `students_in_courses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) unsigned NOT NULL,
  `course_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `student_id` (`student_id`,`course_id`),
  KEY `course_of_student` (`course_id`),
  CONSTRAINT `course_of_student` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `student_in_course` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE
);

DROP TABLE IF EXISTS `student_course_grades`;
CREATE TABLE `student_course_grades` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `student_course_id` int(11) unsigned NOT NULL,
  `grade_id` int(11) unsigned NOT NULL,
  `created_date` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `student_course_id` (`student_course_id`,`grade_id`),
  KEY `grade_representation` (`grade_id`),
  CONSTRAINT `grade_representation` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE,
  CONSTRAINT `student_course_grade` FOREIGN KEY (`student_course_id`) REFERENCES `students_in_courses` (`id`) ON DELETE CASCADE
);

DROP VIEW IF EXISTS `view_student_course_grade`;


INSERT INTO `courses` (`id`, `title`) VALUES
('1', 'Mathe 1'),
('2', 'Mathe 2'),
('3', 'Grundlagen Programmierung 1'),
('4', 'Sport');

INSERT INTO `grades` (`id`, `grade`, `grade_value`, `grade_definition`) VALUES
('2', '+A', '0.6', 'The best'),
('3', 'A', '1', 'Damn good'),
('4', 'A-', '1.3', 'Very good'),
('5', 'B+', '1.6', 'Pretty Good'),
('6', 'B', '2', 'Good'),
('7', 'B-', '2.3', 'Still good'),
('8', 'C+', '2.6', 'More than ok'),
('9', 'C', '3', 'OK'),
('10', 'C-', '3.3', 'Nearly OK');

INSERT INTO `students` (`id`, `name`, `matriculation`) VALUES
('1', 'Max', 'ABC'),
('2', 'Marta', 'BCD'),
('3', 'Merle', 'CDE');

INSERT INTO `students_in_courses` (`id`, `student_id`, `course_id`) VALUES
('1', '1', '1'),
('2', '1', '3'),
('3', '1', '4'),
('4', '2', '2'),
('5', '2', '4'),
('6', '3', '2'),
('7', '3', '3'),
('8', '3', '4');

INSERT INTO `student_course_grades` (`id`, `student_course_id`, `grade_id`, `created_date`) VALUES
('11', '1', '6', '2020-07-18 13:51:38'),
('12', '2', '3', '2020-07-18 13:51:38'),
('13', '3', '7', '2020-07-18 13:51:38'),
('14', '4', '8', '2020-07-18 13:51:38'),
('15', '5', '2', '2020-07-18 13:51:38'),
('16', '6', '5', '2020-07-18 13:52:23'),
('17', '7', '6', '2020-07-18 13:52:23');

CREATE VIEW `view_student_course_grade` AS
    select `students`.`name` AS `student`,`courses`.`title` AS `course`,`grades`.`grade` AS `grade`
    from ((((`student_course_grades` `scg`
        left join `grades` on(`scg`.`grade_id` = `grades`.`id`))
        left join `students_in_courses` `sic` on(`scg`.`student_course_id` = `sic`.`id`))
        left join `courses` on(`sic`.`course_id` = `courses`.`id`))
        left join `students` on(`sic`.`student_id` = `students`.`id`));

