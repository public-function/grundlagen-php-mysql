<?php

class Student{
    /**
     * @var int $id
     */
    var $id;
    /**
     * @var string $name 
     */
    var $name;

	/**
	 * @var string $matriculation
	 */
    var $matriculation;

    /**
     * @var null|Course[] $courses
     */
    var $courses = null;
}