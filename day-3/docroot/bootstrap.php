<?php

require_once __DIR__."/classes/Application.php";
require_once __DIR__."/classes/Database.php";
require_once __DIR__."/classes/Routing.php";
require_once __DIR__."/classes/Renderer.php";