<?php


/**
 * @property mysqli connection
 */
class Database {

	/**
	 * Database constructor.
	 */
	public function __construct() {
		$this->connection = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE);
	}

	/**
	 * @return int
	 */
	public function getStudentsCount() {
		$result = $this->connection->query("SELECT count(id) FROM students");
		$row = $result->fetch_row();
		if(is_array($row) && count($row) == 1){
			return intval($row[0]);
		}
		return 0;
	}

	/**
	 * @return int
	 */
	public function getCoursesCount() {
		$result = $this->connection->query("SELECT count(id) FROM courses");
		$row = $result->fetch_row();
		if(is_array($row) && count($row) == 1){
			return intval($row[0]);
		}
		return 0;
	}

	/**
	 * @return array
	 */
	public function getStudents() {
		$result = $this->connection->query("
			SELECT students.id as id, students.name as name, courses.title as titlte, courses.id as course_id FROM students 
			LEFT JOIN students_in_courses ON ( students.id = students_in_courses.student_id )
			LEFT JOIN courses ON (courses.id = students_in_courses.course_id)
			ORDER BY `name` ASC
		");

		$rows =  $result->fetch_all(MYSQLI_ASSOC);
		$mapped = [];
		$lastStudentId = null;
		foreach ($rows as $row){
			if($lastStudentId !== $row["id"]){
				$lastStudentId = $row["id"];
				$mapped[] = [
					"id" => $row["id"],
					"name" => $row["name"],
					"courses" => [],
				];
			}
			if($row["course_id"] != null){
				$mapped[count($mapped)-1]["courses"][] = [
					"id" => $row["course_id"],
					"title" => $row["title"],
				];
			}
		}
		return $mapped;
	}

	/**
	 * initialize database
	 */
	public function initialize(){
		$this->connection->query("
			CREATE TABLE `students` (
			  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			  `name` varchar(255) NOT NULL,
			  PRIMARY KEY ( id ),
			  KEY (`name`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
		$this->connection->query("
			CREATE TABLE `courses` (
			  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			  `title` varchar(255) NOT NULL,
			  PRIMARY KEY ( id ),
			  key (`title`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
		$this->connection->query("
			CREATE TABLE `students_in_courses` (
			  `student_id` int(11) unsigned NOT NULL,
			  `course_id` int(11) unsigned NOT NULL,
			  PRIMARY KEY (student_id, course_id),
			  CONSTRAINT `student_in_course` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
			  CONSTRAINT `course_of_student` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");

	}




}