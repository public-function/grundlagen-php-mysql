<?php

/**
 * @property Routing routing
 * @property Database database
 * @property Renderer render
 */
class Application{

	/**
	 * @var Database database
	 */
	private $database;

	/**
	 * Application constructor.
	 */
    public function __construct() {
		$this->database = new Database();
		$this->database->getStudentsCount();
		$this->render = new Renderer();
	    $this->routing = new Routing($this->database, $this->render);
    }

	/**
	 * run the application
	 */
    public function run(){
    	$this->database->initialize();
		$this->routing->handle($_REQUEST);
    }

}