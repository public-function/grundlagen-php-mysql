<?php


class Renderer {

	/**
	 * @param array $students
	 */
	public function renderStudents(array $students){
		$this->render(function() use ($students){
			include __DIR__."/../templates/students.php";
		});
	}

	/**
	 * @param int $studentsCount
	 * @param int $coursesCount
	 */
	public function renderHome(int $studentsCount, int $coursesCount){
        $this->render(function() use ($coursesCount, $studentsCount){
            $studentsUrl = "/?page=students";
	        $coursesUrl = "/?page=courses";
            include __DIR__."/../templates/home.php";
        });
    }

	/**
	 * @param callable $body
	 * @param array $args
	 */
	public function render(callable $body, array $args = []){
		$this->header($args);
		$body();
		$this->footer();
	}

	/**
	 * @param array $args
	 */
	public function header(array $args){
	    $title = isset($args["title"]) && !empty($args["title"]) ? $args["title"]: "Kursverwaltung";
		?>
		<!DOCTYPE html>
		<html>
		<head>
			<meta charset="UTF-8">
			<title><?= $title; ?></title>

            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
                  integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

            <link rel="stylesheet" href="/css/style.css">
		</head>
		<body>
		<?php
	}

	private function footer(){
		?>
		</body>
		</html>
		<?php
	}

}