<?php


/**
 * @property Database database
 * @property Renderer render
 */
class Routing {

	const ARG_PAGE = "page";

	const PAGE_HOME = "";
	const PAGE_STUDENTS = "students";
	const PAGE_COURSES = "courses";

	/**
	 * Routing constructor.
	 * @param Database $database
	 * @param Renderer $render
	 */
	public function __construct(Database $database, Renderer $render) {
		$this->database = $database;
		$this->render = $render;
	}

	/**
	 * @param array $arguments
	 */
	public function handle(array $arguments) {
		$page = (isset($arguments[self::ARG_PAGE]))? $arguments[self::ARG_PAGE]: "";
		switch ($page){
			case self::PAGE_HOME:
				$this->routeHome($arguments);
				break;
			case self::PAGE_STUDENTS:
				$this->routeStudents($arguments);
				break;
			default:
				$this->route404();
				break;
		}
	}

	public function routeHome(array $arguments){
	    $students = $this->database->getStudentsCount();
	    $courses = $this->database->getCoursesCount();
	    $this->render->renderHome($students, $courses);
	}

	public function routeStudents(array $arguments){
        $students = $this->database->getStudents();
        $this->render->renderStudents($students);
	}

	public function route404(){
		$this->render->render(function(){
			?>
            Hier bist du falsch. <a href="/">Zurück zum Start</a>
			<?php
		});
	}

}