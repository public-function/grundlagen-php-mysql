<?php

require_once dirname(__FILE__)."/config.php";
require_once dirname(__FILE__)."/bootstrap.php";

// -- this is where is all starts ---
$app = new Application();
$app->run();