<?php
/**
 * @var int $studentsCount
 * @var int $studentsUrl
 * @var int $coursesCount
 * @var string $coursesUrl
 *
 */
?>
<div class="row">
	<div class="col-md-6"><a href="<?= $studentsUrl; ?>"><?= $studentsCount; ?> Studenten</a></div>
	<div class="col-md-6"><a href="<?= $coursesUrl; ?>"><?= $coursesCount; ?> Kurse</a></div>
</div>