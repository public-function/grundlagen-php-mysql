<?php
/**
 * @var string $students
 */
?>

<table id="table-students" class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Kurse</th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach ($students as $student){
        $name = $student["name"];
        $coursesCount = count($student["courses"]);
        echo "<tr>";
        echo "<td>$name</td>";
        echo "<td>$coursesCount</td>";
        echo "</tr>";
    }
    ?>

    </tbody>
</table>