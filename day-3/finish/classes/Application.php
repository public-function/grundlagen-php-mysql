<?php

/**
 * @property Router $router
 * @property Database database
 * @property Template template
 */
class Application{

	/**
	 * @var Database database
	 */
	private $database;

	/**
	 * Application constructor.
	 */
    public function __construct() {
		$this->database = new Database();
		$this->template = new Template();
    }

	/**
	 * run the application
	 */
    public function run(){

    	$baseRouter = new Router("page");

    	$this->router->GET("", function(){

	    });

    	$this->router->GET("students", function(){

	    });

    	$this->router->GET("courses", function(){


	    });


    	$this->database->initialize();
		$this->router->handle($_REQUEST);
    }

}