<?php

require_once __DIR__."/classes/Application.php";
require_once __DIR__."/classes/Database.php";
require_once __DIR__."/classes/Router.php";
require_once __DIR__."/classes/Template.php";
require_once __DIR__."/classes/Mail.php";