<?php

// --------------------------------------
// setup application
// --------------------------------------
require_once __DIR__."/bootstrap.php";
$db = new Database();

// --------------------------------------
// handle insert single student request
// --------------------------------------
$newStudentId = false;
if(
        isset($_POST["insert_student_name"]) && !empty($_POST["insert_student_name"])
    &&
        isset($_POST["insert_student_matrikel"]) && !empty($_POST["insert_student_matrikel"])
){
	$newStudentId = $db->addStudent( $_POST["insert_student_matrikel"] , $_POST["insert_student_name"] );
}

include __DIR__."/templates/body-before.php";

?>

<p><a href="index.php">Zurück zur Übersicht</a></p>

<div class="row">
    <div class="col-md-6">
        <form method="GET">
            <h1>Student suchen</h1>
            <label>
                Name<br/>
                <?php
                // --------------------------------------
                // restore search query
                // --------------------------------------
                $query = "";
                if(isset($_GET["find_student_name"])){
                    $query = htmlspecialchars($_GET["find_student_name"]);
                }
                ?>
                <input type="text" name="find_student_name" value="<?php echo $query; ?>" />
            </label>
            <button>Filtern</button>
        </form>
    </div>
    <div class="col-md-6">
        <form method="POST">
            <h1>Neuer Student</h1>
            <label>
                Matrikelnummer<br/>
                <input type="text" name="insert_student_matrikel" />
            </label>
            <label>
                Name<br/>
                <input type="text" name="insert_student_name" />
            </label>
            <button>Speichern</button>
            <?php
            // --------------------------------------
            // render change info
            // --------------------------------------
            if($newStudentId && isset($_POST["insert_student_name"]) && isset($_POST["insert_student_matrikel"])){
                $name = htmlspecialchars($_POST["insert_student_name"]);
                $matrikel = htmlspecialchars($_POST["insert_student_matrikel"]);
	            echo "<p>Neuer Student '$name' mit Matrikelnummer $matrikel wurde hinzugefügt.</p>";
            }
            ?>
        </form>
    </div>
</div>

<?php

// --------------------------------------
// find requested data
// --------------------------------------
$students = [];
if(isset($_GET["find_student_name"]) && !empty($_GET["find_student_name"])){
    $students = $db->findStudents($_GET["find_student_name"]);
} else {
    $students = $db->getStudents();
}

// --------------------------------------
// render requested data
// --------------------------------------
echo "<table class='table'>";
    echo "<tr>";
    echo "<th style='width: 160px'>Matrikelnummer</th>";
    echo "<th>Name</th>";
echo"</tr>";

foreach($students as $student){
    echo "<tr>";
    echo "<td>".$student->matriculation."</td>";
    echo "<td>".$student->name."</td>";
    echo "</tr>";
}
echo "</table>";

include __DIR__."/templates/body-after.php";

?>

