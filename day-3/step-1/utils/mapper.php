<?php

/**
 * @param array $row
 * @return Student
 */
function mapRowToStudent($row){
	$student = new Student();
	$student->id = $row["id"];
	$student->name = $row["name"];
	$student->matriculation = $row["matriculation"];
	return $student;
}

/**
 * @param array $row
 * @return Course
 */
function mapRowToCourse($row){
	$course = new Course();
	$course->title = $row["title"];
	$course->id = $row["id"];
	return $course;
}