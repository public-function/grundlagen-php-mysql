<?php

// --------------------
// functions
// --------------------
echo "<br/><br/>FUNCTIONS:";

/**
 * @param int $number1
 * @param int $number2
 */
function addition($number1, $number2){
	return $number1 + $number2;
}

echo "<br>Addition: ";
echo addition(2,3);


echo "<br>Password generator: ";
function generatePassword($length = 10){
	$candidateChars = str_split("abcdefghijklmnopqrstvwxyz1234567890:!+");
	$result = [];
	while(count($result) < $length){
		$result[] = $candidateChars[rand(0, count($candidateChars) - 1)];
	}
	return implode("",$result);
}
echo generatePassword();

// --------------------
// classes
// --------------------
echo "<br><br>CLASSES:";

echo "<br>Calculator:";

/**
 * @property int value
 */
class Calculator{

	private $value;

	public function __construct(int $value = 0) {
		$this->value = $value;
	}

	public function getValue(){
		return $this->value;
	}

	public function add($value){
		$this->value += $value;
	}

	public function subtract($value){
		$this->value -= $value;
	}
}

$calculator = new Calculator();
$calculator->add(2);
$calculator->subtract(5);
$calculator->add(10);
echo $calculator->getValue();

echo "<br>PasswordGenerator:";

class PasswordGenerator{
	private $chars = "abcdefghijklmnopqrstvwxyz1234567890:!+";
	private $length = 10;

	/**
	 * @param string $chars
	 * @return PasswordGenerator
	 */
	public function setChars($chars){
		$this->chars = $chars;
		return $this;
	}

	/**
	 * @param int $length
	 * @return PasswordGenerator
	 */
	public function setLength($length){
		$this->length = $length;
		return $this;
	}

	public function generate(){
		$candidateChars = str_split($this->chars);
		$result = [];
		while(count($result) < $this->length){
			$result[] = $candidateChars[rand(0, count($candidateChars) - 1)];
		}
		return implode("",$result);
	}
}

$userPasswordGenerator = new PasswordGenerator();
$userPasswordGenerator->setChars("ABC");
$userPasswordGenerator->setLength(20);
$coursePasswordGenerator = new PasswordGenerator();
$coursePasswordGenerator->setChars("XYZ")->setLength(5);

echo "<br>UserPassword ABS 20: ".$userPasswordGenerator->generate();
echo "<br>CoursePassword XYZ 5: ".$coursePasswordGenerator->generate();
echo "<br>UserPassword ABS 20: ".$userPasswordGenerator->generate();
echo "<br>CoursePassword XYZ 5: ".$coursePasswordGenerator->generate();


echo "<br>Model Class:";
class Person{
	/**
	 * @var string $name name of this person
	 */
	var $name;
	/**
	 * @var int $age age of this person
	 */
	var $age;

	/**
	 * @var string $job the job of this person
	 */
	var $job = "none";
	public function __construct($name, $age) {
		$this->name = $name;
		$this->age = $age;
	}
}
$person = new Person("Max Mustermann", 21);

echo "<br/>Name: ".$person->name;
echo "<br/>Age: ".$person->age;
echo "<br/>Job: ".$person->job;
$person->job = "Programmer";
echo "<br/>Job: ".$person->job;
