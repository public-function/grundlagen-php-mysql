<?php
require_once dirname(__FILE__)."/bootstrap.php";

?><!DOCTYPE html>
<html>
<head>  
    <meta charset="UTF-8">
    <title>HTML with PHP Template</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" 
        href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" 
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous"
    />
</head>
<body style="padding: 10px;">
<?php

echo "<h1>Hello Classes!</h1>";

$calculator = new Calculator(2);
//$calculator->value = 2;
$calculator->addition(2);
$calculator->addition(10);
$calculator->subtract(4);
$value = $calculator->getValue();
echo "<p>C1 $value</p>";

$calculator2 = new Calculator(3);
$calculator2->addition(2);
$value2 = $calculator2->getValue();
echo "<p>C2 $value2</p>";

?>
</body>
</html>

