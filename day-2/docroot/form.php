<?php
require_once dirname(__FILE__)."/bootstrap.php";

?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>HTML with PHP Template</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
          integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous"
    />
</head>
<body style="padding: 10px;">
<?php

echo "<h1>Hello Form!</h1>";

?>
<form method="post">
    <input name="message" type="text" />
    <button>Send</button>
</form>

<?php

$filename = __DIR__."/text.txt";

// write message to file if form was submitted
if( isset($_POST["message"]) && !empty($_POST["message"]) ){
	$msg = htmlspecialchars($_POST["message"]);

	$content = getMessages($filename);
	file_put_contents($filename, $msg."\n".$content);
}

// read from file and display messages
$content = getMessages($filename);
if( empty($content) ){
	echo "<p>Keine Nachricht vorhanden.</p>";
} else {
	echo "<p>". nl2br($content)."</p>";
}

?>
</body>
</html>

