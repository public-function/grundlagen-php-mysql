<?php

require_once dirname(__FILE__)."/bootstrap.php";

$db = new Database();

// ---------------------------------------------------
// check if course was selected
// ---------------------------------------------------
$selectedCourseId = false;
if(isset($_GET["course_id"])){
	$selectedCourseId = intval($_GET["course_id"]);
}


// ---------------------------------------------
// add student to course if form was submitted
// ---------------------------------------------
if(
	isset($_POST["add_student_id"]) && !empty($_POST["add_student_id"])
    &&
	isset($_POST["add_course_id"]) && !empty($_POST["add_course_id"])
){
	$studentId = intval($_POST["add_student_id"]);
	$courseId = intval($_POST["add_course_id"]);
	$db->takeCourse($studentId, $courseId);
}

// ---------------------------------------------------
// remove student from course if form was submitted
// ---------------------------------------------------
if(
	isset($_POST["remove_student_id"]) && !empty($_POST["remove_student_id"])
    &&
	isset($_POST["remove_course_id"]) && !empty($_POST["remove_course_id"])
){
	$studentId = intval($_POST["remove_student_id"]);
	$courseId = intval($_POST["remove_course_id"]);
	$db->exitCourse($studentId, $courseId);
}


?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>HTML with PHP Template</title>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" 
    href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" 
    integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous"
    />
</head>
<body style="padding: 10px;">

<form method="GET">
    <label>
        Kurs<br/>
        <select name="course_id">
            <option value="">-- Kurs auswählen --</option>
            <?php
            $courses = $db->getCourses();
            foreach($courses as $course){
                $selected = "";
                if($selectedCourseId == $course->id){
                    $selected = "selected";
                }
                echo "<option $selected value='$course->id'>$course->title</option>";
            }
            ?>
        </select>
    </label>
    <button>Auswählen</button>
</form>

<?php

if(false != $selectedCourseId){


    ?>
    <form method="POST">
        <input type="hidden" value="<?php echo $selectedCourseId; ?>" name="add_course_id" />
        <label>
            Student<br/>
            <select name="add_student_id">
                <?php
                $students = $db->getStudents();
                foreach($students as $student){
                    
                    $isInCourse = $db->isInCourse($student->id, $selectedCourseId);
                    if($isInCourse) continue;

                    echo "<option value='$student->id'>$student->name</option>";
                }
                ?>
            </select>
        </label>
        <button>Hinzufügen</button>
    </form>
    <?php
    $students = $db->getStudentsInCourse($selectedCourseId);

    echo "<table class='table'>";
    echo "<tr><th>Id</th><th>Name</th><th>Action</th></tr>";
    foreach($students as $student){
        echo "<tr>";
        echo "<td>$student->id</td><td>$student->name</td>";
        ?>
        <td>
            <form method="post">
                <input type="hidden" value="<?php echo $selectedCourseId; ?>" name="remove_course_id" />
                <input type="hidden" value="<?php echo $student->id ?>" name="remove_student_id" />
                <button>Aus Kurs entfernen</button>
            </form>
        </td>
        <?php
        echo "</tr>";
    }
    echo "</table>";
} else {
    echo "<p>Wähle einen Kurs, dann werden alle Studenten in diesem Kurs aufgelistet.";
}


?>
</body>
</html>

