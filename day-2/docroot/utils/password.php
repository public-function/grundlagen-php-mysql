<?php

/**
 * @param integer $length length of password
 * @return string calculated result
 *
 */
function passwordGenerator(
	$length = 10,
	$chars = "abcdefghijklmnopqrstvwxyz1234567890:!+"
){

	$candidateChars = str_split($chars);
	// $candidateChars = ["a","b","c"...]
	$result = [];
	for($i = 0; $i < $length; $i++){
		$result[$i] = $candidateChars[rand(0, count($candidateChars) - 1)];
	}
	return implode("",$result);
}