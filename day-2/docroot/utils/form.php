<?php

function getMessages($filename){
	$content = "";
	if(file_exists($filename)){
		$content = file_get_contents($filename);
	}
	return $content;
}