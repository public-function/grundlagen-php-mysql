<?php

/**
 * @return mysqli
 */
function getConnection(){
	return new mysqli("localhost", "butler", "butler", "butler");
}

/**
 * @param mysqli $connection
 * @return object[]
 */
function getStudents($connection){
	$result = $connection->query("SELECT * FROM students ORDER BY id DESC");
	$rows = $result->fetch_all(MYSQLI_ASSOC);
	return $rows;
}

/**
 * @param mysqli $connection
 * @param string $query
 * @return object[]
 */
function findStudents($connection, $query){
	$query = $connection->real_escape_string($query);
	$result = $connection->query("SELECT * FROM students WHERE `name` LIKE '%$query%' ORDER BY `name` ASC");
	return $result->fetch_all(MYSQLI_ASSOC);
}

/**
 * @param mysqli $connection
 * @param string $name
 * @return int|false
 */
function addStudent($connection, $name){
	$name = $connection->real_escape_string($name);
	$success = $connection->query("INSERT INTO students (`name`) VALUES ('$name')");
	if(!$success) return false;
	return $connection->insert_id;
}

/**
 * @param mysqli $connection
 * @param int $studentId
 * @param int $courseId
 * @return bool
 */
function takeCourse($connection, $studentId, $courseId){
	$studentId = intval($studentId);
	$courseId = intval($courseId);
	return $connection->query("INSERT INTO students_in_courses (student_id, course_id) VALUES ($studentId, $courseId)");
}

/**
 * @param mysqli $connection
 * @param int $studentId
 * @param int $courseId
 * @param int $grade
 *
 * @return boolean
 */
function setGrade($connection, $studentId, $courseId, $grade){
	// if connection gets accessed from outside this is a "side effect"
	$studentId = intval($studentId);
	$courseId = intval($courseId);
	$grade = intval($grade);
	return $connection->query("UPDATE students_in_courses SET grade = $grade WHERE student_id = $studentId AND course_id = $courseId");
}