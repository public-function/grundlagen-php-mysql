## From day 1

CREATE TABLE `students` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY ( id ),
  KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `courses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY ( id ),
  key (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `students_in_courses` (
  `student_id` int(11) unsigned NOT NULL,
  `course_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (student_id, course_id),
  CONSTRAINT `student_in_course` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE,
  CONSTRAINT `course_of_student` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SELECT
	courses.title as course,
	students.name as student
FROM students
	LEFT JOIN students_in_courses ON ( students.id = students_in_courses.student_id )
	LEFT JOIN courses ON (courses.id = students_in_courses.course_id)
	ORDER BY students.name ASC;

CREATE TABLE `students_in_courses` (
  id int(11) unsigned NOT NULL, ## With ID
  `student_id` int(11) unsigned NOT NULL,
  `course_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (student_id, course_id),
  CONSTRAINT `student_in_course` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE,
  CONSTRAINT `course_of_student` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



## add student course notes

ALTER TABLE students_in_courses ADD COLUMN `grade` int(1);

## get all students with grade 1
SELECT
	courses.title as course,
	students.name as student,
	students_in_courses.grade as grade
FROM students
	LEFT JOIN students_in_courses ON ( students.id = students_in_courses.student_id )
	LEFT JOIN courses ON (courses.id = students_in_courses.course_id)
	WHERE students_in_courses.grade = 1
	ORDER BY students.name ASC;

## add student course multiple notes

CREATE TABLE `students_in_courses` (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) unsigned NOT NULL,
  `course_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (student_id, course_id),
  CONSTRAINT `student_in_course` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE,
  CONSTRAINT `course_of_student` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `grades` (
  id int(11) unsigned AUTO_INCREMENT,
  `grade` varchar(10) NOT NULL,
  `grade_definition` text DEFAULT '',
  PRIMARY KEY (id),
  UNIQUE KEY (grade)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `student_course_grades` (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  `student_course_id` int(11) unsigned NOT NULL,
  `grade_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (id),
  KEY (student_course_id, grade_id), ## with unique key just one grade per course
  CONSTRAINT `student_course_grade` FOREIGN KEY (`student_course_id`) REFERENCES `students_in_courses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `grade_representation` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


## Views

CREATE VIEW view_student_course_grade AS 
  select `students`.`name` AS `student`,`courses`.`title` AS `course`,`grades`.`grade` AS `grade` 
  from `student_course_grades` as `scg` 
  left join `grades` on(`scg`.`grade_id` = `grades`.`id`)
  left join `students_in_courses` as `sic` on(`scg`.`student_course_id` = `sic`.`id`)
  left join `courses` on(`sic`.`course_id` = `courses`.`id`)
  left join `students` on(`sic`.`student_id` = `students`.`id`);


