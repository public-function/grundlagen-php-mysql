<?php

require_once __DIR__."/bootstrap.php";

/**
 * @param Fighter $attacker
 * @param Fighter $victim
 * @param int $damage
 * @return string
 */
function echoAttackMessage($attacker, $victim, $damage){
	echo "<p>$attacker->name ⚔️ $victim->name $damage 🩸</p>";
}

/**
 * @param Fighter $player
 * @return string
 */
function echoWinsMessage($player){
    echo "<p>$player->name wins 🎉</p>";
}

function echoStateOfGame($player1, $player2){
	echo "<p>".$player1->name." ".$player1->health." ❤️ | ".$player2->name." ".$player2->health." ❤️</p>";
	echo "<hr>";
}

echo "Little Game:";

if(isset($_POST["player1"])  && isset($_POST["player2"])){

	$player1 = new Fighter($_POST["player1"]);
	$player2 = new Fighter($_POST["player2"]);

	while(true){
		$damage = $player1->attack($player2);
		echoAttackMessage($player1, $player2, $damage);
		if($player2->isDead()){
			echoWinsMessage($player1);
			break;
		}

		$damage = $player2->attack($player1);
		echoAttackMessage($player2, $player1, $damage);
		if($player1->isDead()){
			echoWinsMessage($player2);
			break;
		}

        echoStateOfGame($player1, $player2);

	}

	echo "<p><a href='/game.php'>New match</a></p>";

} else {
	?>
	<form method="post">
		<div>
			<label>Player 1 <input name="player1" /></label>
		</div>
		<div>
			<label>Player 2 <input name="player2" /></label>
		</div>
		<button>Play</button>
	</form>
	<?php
}

