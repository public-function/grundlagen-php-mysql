<?php

// setup base
require dirname(__FILE__)."/utils/database.php";
$connection = getConnection();

$newStudentId = false;
if(isset($_POST["student_name"]) && !empty($_POST["student_name"])){
	$name = $connection->real_escape_string($_POST["student_name"]);
	$newStudentId = addStudent($connection, $name);
}

?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Students List - with Functions</title>
    <!-- Latest compiled and minified CSS -->
    <link 
        rel="stylesheet" 
        href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" 
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous"
    />
</head>
<body style="padding: 10px;">
<?php

// https://www.php.net/manual/de/reserved.variables.files.php
// $_FILES = [
//  "import" => [
//      "tmp_name" => "/path/to/tmp/file.txt",
//      ...
//  ]    
//]
if(isset($_FILES["import"]) && file_exists($_FILES["import"]["tmp_name"])){
    $content = file_get_contents($_FILES["import"]["tmp_name"]);
    $exploded = explode(';', $content);
    foreach($exploded as $item){
        if(!empty($item)){
            addStudent($connection, $item);
        }
    }
}
?>

<div class="row">
    <div class="col-md-4">
        <form method="GET">
            <h1>Student suchen</h1>
            <label>
                Name<br/>
                <?php
                $query = "";
                if(isset($_GET["student_name"])){
                    $query = htmlspecialchars($_GET["student_name"]);
                }
                ?>
                <input type="text" name="student_name" value="<?php echo $query; ?>" />
            </label>
            <button>Filtern</button>
        </form>
    </div>
    <div class="col-md-4">
        <form method="POST">
            <h1>Neuer Student</h1>
            <label>
                Name<br/>
                <input type="text" name="student_name" />
            </label>
            <button>Speichern</button>
            <?php
            if($newStudentId){
	            echo "<p>Neuer Student '$name' wurde hinzugefügt.</p>";
            }
            ?>
        </form>
    </div>
    <div class="col-md-4">
        <form method="POST" enctype="multipart/form-data">
            <h1>Studenten importieren</h1>
            <input type="file" name="import" />
            <button>Importieren</button>
        </form>
    </div>
</div>

<?php

$students = [];
if(isset($_GET["student_name"]) && !empty($_GET["student_name"])){
    $students = findStudents($connection, $_GET["student_name"]);
} else {
    $students = getStudents($connection);
}

echo "<table class='table'>";
    echo "<tr><th>ID</th><th>Name</th></tr>";

foreach($students as $student){
    echo "<tr>";
    echo "<td>".$student["id"]."</td>";
    echo "<td>".$student["name"]."</td>";
    echo "</tr>";
}
echo "</table>";

?>
</body>
</html>

