<?php

/**
 * @property string name
 * @property int health
 */
class Fighter{
	var $health = 100;
	var $damageFrom = 1;
	var $damageTo = 15;
	public function __construct($name) {
		$this->name = $name;
	}
	public function takeDamage($value){
		$this->health -= $value;
	}

	public function attack(Fighter $fighter){
		$damage = random_int($this->damageFrom, $this->damageTo);
		$fighter->takeDamage($damage);
		return $damage;
	}

	public function isDead(){
		return $this->health <= 0;
	}
}