<?php


class Calculator {
	private $value; //property

	public function __construct($initialValue = 0) {
		$this->value = $initialValue;
	}

	function addition($addValue){ // method
		$this->value = $this->value + $addValue;
	}

	function subtract($subValue){
		$this->value = $this->value - $subValue;
	}

	function getValue(){
		return $this->value;
	}

}