<?php

class Course{
    /**
     * @var int $id
     */
    var $id;
    /**
     * @var string $title
     */
    var $title;

    /**
     * @var null|Student[] $participants
     */
    var $participants = null;
}