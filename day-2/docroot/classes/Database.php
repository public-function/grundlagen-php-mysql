<?php

/**
 * Class Database
 */
class Database{

	/**
	 * @var mysqli
	 */
	private $connection;

	/**
	 * Database constructor.
	 */
	public function __construct() {
		$this->connection = new mysqli("localhost", "butler", "butler", "butler");
	}

	/**
	 * @param string $name
	 * @return int|false
	 */
	function addStudent($name){
		$name = $this->connection->real_escape_string($name);
		$success = $this->connection->query("INSERT INTO students (`name`) VALUES ('$name')");
		if(!$success) return false;
		return $this->connection->insert_id;
    }
    
    /**
     * @return Student[]
     */
    function getStudents(){
        $result = $this->connection->query("SELECT * FROM students");
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        
        if(!is_array($rows)) return [];

        $students = [];
        foreach($rows as $row){
            $student = new Student();
            $student->id = $row["id"];
            $student->name = $row["name"];
            $students[] = $student;
        }
        return $students;
	}
	
	/**
	 * @return Course[]
	 */
	function getCourses(){
		$result = $this->connection->query("SELECT * FROM courses");
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        
        if(!is_array($rows)) return [];

		return array_map(function($row){
			$course = new Course();
			$course->title = $row["title"];
			$course->id = $row["id"];
			return $course;
		}, $rows);
	}

	/**
	 * @param int $student_id
	 * @param int $course_id
	 * @return boolean
	 */
	function isInCourse($student_id, $course_id){
		$student_id = intval($student_id);
		$course_id = intval($course_id);
		$result = $this->connection->query("SELECT count(*) as `count` FROM students_in_courses WHERE student_id = $student_id AND course_id = $course_id");
		$field = intval($result->fetch_object()->count);
		return $field > 0;
	}

	/**
	 * @param int $studentId
	 * @param int $courseId
	 * @return bool
	 */
	function takeCourse($studentId, $courseId){
		$studentId = intval($studentId);
		$courseId = intval($courseId);
		return $this->connection->query("INSERT INTO students_in_courses (student_id, course_id) VALUES ($studentId, $courseId)");
	}

	/**
	 * @param int $studentId
	 * @param int $courseId
	 * @return bool
	 */
	function exitCourse($studentId, $courseId){
		$studentId = intval($studentId);
		$courseId = intval($courseId);
		$query = "DELETE FROM students_in_courses WHERE course_id = $courseId AND student_id = $studentId";
		return $this->connection->query($query);
	}
	
	/**
	 * @param int $course_id
	 * @return Student[]
	 */
	function getStudentsInCourse($course_id){
		$course_id = intval($course_id);
		$result = $this->connection->query(
            "SELECT student_id, `name` FROM students_in_courses as sic ".
            "LEFT JOIN students ON (sic.student_id = students.id)".
            "WHERE course_id = $course_id;"
        );
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        
        if(!is_array($rows)) return [];

        return array_map(function($row){
            $item = new Student();
            $item->id = $row["student_id"];
            $item->name = $row["name"];
            return $item;
        },$rows);
	}
    
    /**
     * @param int $studentId
     * @return Course[]
     */
    function getCoursesForStudent($studentId){
        $studentId = intval($studentId);
        $result = $this->connection->query(
            "SELECT course_id, title FROM students_in_courses as sic ".
            "LEFT JOIN courses ON (sic.course_id = courses.id)".
            "WHERE student_id = $studentId;"
        );
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        
        if(!is_array($rows)) return [];

        return array_map(function($row){
            $item = new Course();
            $item->id = $row["course_id"];
            $item->title = $row["title"];
            return $item;
        },$rows);
    }

	/**
	 * @param int $studentId
	 * @param int $courseId
	 * @param int $grade
	 *
	 * @return boolean
	 */
	function setGrade($studentId, $courseId, $grade){
		// if connection gets accessed from outside this is a "side effect"
		$studentId = intval($studentId);
		$courseId = intval($courseId);
		$grade = intval($grade);
		return $this->connection->query("UPDATE students_in_courses SET grade = $grade WHERE student_id = $studentId AND course_id = $courseId");
	}

}