<?php

require_once __DIR__."/utils/calculator.php";
require_once __DIR__."/utils/password.php";
require_once __DIR__."/utils/form.php";
require_once __DIR__."/utils/database.php";

require_once dirname(__FILE__)."/classes/Calculator.php";
require_once dirname(__FILE__)."/classes/Student.php";
require_once dirname(__FILE__)."/classes/Course.php";
require_once dirname(__FILE__)."/classes/Database.php";
require_once dirname(__FILE__)."/classes/Fighter.php";