CREATE TABLE `table1` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `short_text` varchar(255) NOT NULL DEFAULT '',
  `long_text` TEXT,
  `double_value` double NOT NULL DEFAULT '0',
  `float_value` float DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO table1
    (short_text, long_text, double_value, float_value)
VALUES
    ('short text value', 'Long text value', 1.3, 1.4);

SELECT short_text FROM table1 WHERE double_value > 1;

CREATE TABLE `table2` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `table1_id` int(11) unsigned NOT NULL,
  `attribute_key` varchar(255) NOT NULL DEFAULT '',
  `attribute_value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `table1_to_table2` (`table1_id`),
  CONSTRAINT `table1_to_table2` FOREIGN KEY (`table1_id`) REFERENCES `table1` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ##  Step 1 schedule not normalized

CREATE TABLE `schedule_not_normalized` (
  `student_name` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  PRIMARY KEY (`student_name`, `course`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ##  Step 1 students normalized but schedule not normalized

CREATE TABLE `students` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY ( id ),
  KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `courses_not_normalized` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) unsigned NOT NULL,
  `course` varchar(255) NOT NULL,
  PRIMARY KEY ( id ),
  key (`student_id`),
  UNIQUE KEY ( student_id, course),
  CONSTRAINT `student_in_not_normalized_course` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ## Select all student courses

SELECT * FROM students
	LEFT JOIN courses_not_normalized ON (courses_not_normalized.student_id = students.id);

-- ##  Step 2 students and schedule normalized

CREATE TABLE `courses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY ( id ),
  key (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `students_in_courses` (
  `student_id` int(11) unsigned NOT NULL,
  `course_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (student_id, course_id),
  CONSTRAINT `student_in_course` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE,
  CONSTRAINT `course_of_student` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ## SELECT all students with their courses

SELECT * FROM students 
	LEFT JOIN students_in_courses ON ( students.id = students_in_courses.student_id )
	LEFT JOIN courses ON (courses.id = students_in_courses.course_id);

-- ## SELECT all courses with their students

SELECT * FROM courses 
	LEFT JOIN students_in_courses ON ( courses.id = students_in_courses.course_id )
	LEFT JOIN students ON (students.id = students_in_courses.student_id);

-- ## select all user course connections

SELECT * FROM courses 
	INNER JOIN students_in_courses ON ( courses.id = students_in_courses.course_id )
	INNER JOIN students ON (students.id = students_in_courses.student_id);
