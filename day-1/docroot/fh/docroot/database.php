<?php

$connection = new mysqli("localhost", "root", "", "<yourdatabasename>");

// select all rows from students table
$result = $connection->query("SELECT * FROM students");

// this is what is saved to $rows
$rows = $result->fetch_all(MYSQLI_ASSOC);
//$rows = [
//	[
//		"id" => 2,
//		"name" => "Ben",
//	],
//	[
//		"id" => 1,
//	    "name" => "Edward",
//	],
//  ...
//];
foreach ($rows as $row){
	echo "<br/>ID: ".$row["id"]." - Name: ".$row["name"];
}

// Insert new entry (change 'mykey' to add new entry)
$result = $connection->query("INSERT INTO students VALUES (null, 'mykey', 'Name', 'Lastname')");