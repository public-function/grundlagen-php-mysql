<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Grundlagen PHP und MySQL</title>
</head>
<body>
    <h1>
    <?php
    echo "Hello World!";
    ?>
    </h1>
    <p><a href="showcase.php">Showcase</a></p>
    <p><a href="basics.php">Basics</a></p>
    <p><a href="mysql.php">Connection to MySQL</a></p>
    <p><a href="game.php">Little Game</a></p>
</body>
</html>