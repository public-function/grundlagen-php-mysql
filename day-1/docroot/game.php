<?php

/**
 * @property string name
 * @property int health
 */
class Fighter{
	var $health = 100;
	var $damageFrom = 10;
	var $damageTo = 15;
	public function __construct($name) {
		$this->name = $name;
	}
	public function takeDamage($value){
		$this->health -= $value;
	}

	public function attack(Fighter $fighter){
		$damage = random_int($this->damageFrom, $this->damageTo);
		$fighter->takeDamage($damage);
		return $damage;
	}

	public function isDead(){
		return $this->health <= 0;
	}
}

echo "Little Game:";

if(isset($_POST["player1"])  && isset($_POST["player2"])){


	$player1 = new Fighter($_POST["player1"]);
	$player2 = new Fighter($_POST["player2"]);

	while(true){
		$damage = $player1->attack($player2);
		echo "<br/><br/>".$player1->name." damaged ".$player2->name." ".$damage." points";
		if($player2->isDead()){
			echo "<br/><br/>".$player1->name." wins 🎉";
			break;
		}
		$damage = $player2->attack($player1);
		echo "<br/>".$player2->name." damaged ".$player1->name." ".$damage." points";
		if($player1->isDead()){
			echo "<br/><br/>".$player2->name." wins 🎉";
			break;
		}

		echo "<br/>".$player1->name." ".$player1->health."❤️ | ".$player2->name." ".$player2->health."❤️";

	}

	echo "<p><a href='/game.php'>New match</a></p>";

} else {
	?>
	<form method="post">
		<div>
			<label>Player 1 <input name="player1" /></label>
		</div>
		<div>
			<label>Player 2 <input name="player2" /></label>
		</div>
		<button>Play</button>
	</form>
	<?php
}

