<?php
// --------------------
// string
// --------------------
echo "string: ";

$var1 = "Hello World";

echo $var1;

// --------------------
// integer
// --------------------
echo "<br/>integer: ";

$number = 1;

echo " " . $number;
echo " + 1 = " . ($number + 1);

// --------------------
// float
// --------------------
echo "<br/>float: ";

$float = 1.2;

echo " " . $float;
echo " + 1 = " . ($float + 1.3);

// --------------------
// array
// --------------------
$indexArray = [10,20,50,80];
echo "<br/>";
print_r($indexArray);
echo "<br/> Index 2: ".$indexArray[1];

$assocArray = array(
	"prop1" => "value1",
	"prop2" => "Value 2",
);
echo "<br/>";
print_r($assocArray);
echo "<br/>prop2: ".$assocArray["prop2"];

// --------------------
// conditions
// --------------------
echo "<br/><br/>Conditions:";
echo "<br/>if: ";

$isTrue = false;
if ($isTrue) {
	echo "Yes this is true";
} else {
	echo "No this is not true";
}

$number = 1;
if ($isTrue && $number > 2) {
	echo "Number is greater 2";
}
if ($isTrue || $number > 2) {
	echo "Number could be greater 2";
}

echo "<br/>switch: ";
$state = 1;
switch ($state) {
	case 1:
		echo "Ready...";
		break;
	case 2:
		echo "Set...";
		break;
	case 3:
		echo "Go!";
		break;
	default:
		echo "Unknown state: $number";
}

// --------------------
// loops
// --------------------
echo "<br/><br/>Loops:";

echo "<br/>for: ";

for ($i = 0; $i <= 10; $i++) {
	echo " " . $i;
}

echo "<br/>foreach: ";

$person = [
	"name" => "Max",
	"lastname" => "Mustermann",
	"gender" => "male",
	"hairColor" => "black",
];

foreach ($person as $prop => $value) {
	echo  $prop . " => " . $value . " ::: ";
}

echo "<br/>while: ";
$counter = 0;
while ($counter > 0) {
	echo $counter . "... ";
	$counter--;
}

echo "<br/>do while: ";

$index = 0;
$arr = ["one", "two", "three"];
do {
	$index++;
	if (count($arr) <= $index) {
		echo "no item left";
	} else {
		echo $arr[$index] . " ::: ";
	}
} while (count($arr) > $index);


