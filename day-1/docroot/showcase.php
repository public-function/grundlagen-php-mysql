<?php
echo "Hallo Welt";

echo "<br/>";

$var1 = "TEST";

echo $var1;

echo "<br/>";

$myNumber = 1;

echo $myNumber;

echo "<br/>";

$myFloat = 1.2;

echo $myFloat;

$myBool = false;

echo "<br/>";

echo $myBool;

// ---arrays ----

echo "<br/>";

$myArray = array("wert1", "wert2", "wert3", 1, 2, 3);
$myArray = ["wert1", "wert2", "wert3", 1, 2, 3];
var_dump($myArray);
echo "<br/>";

echo $myArray[2];

echo "<br/>";

$myAssocArray = [
	"name" => "Max",
	"lastname" => "Mustermann",
	"age" => 30,
];

$property = "name";

echo $myAssocArray[$property];

$listOfStudents = [
	[
		"name" => "Max",
	],
	[
		"name" => "Edward"
	]
];

$listOfStudents[0]["name"];

echo "<br/>";

for($i = 0; $i < 10; $i++){
	echo "$i index --";
}

$myArray = array("wert1", "wert2", "wert3", 1, 2, 3);

echo "<br/>";

for ($i = 0; $i < count($myArray); $i++){
	$var = $myArray[$i];
	echo "$var --";
}

$myAssocArray = [
	"name" => "Max",
	"lastname" => "Mustermann",
	"age" => 30,
	"grade" => 3
];

echo "<br/>";

foreach ($myAssocArray as $key =>  $value){
	echo "$key = $value ---";

	if($key == "grade" AND $value > 3 ){
		echo "Should be worried";
	} else if($key == "grade") {
		echo "It's fine";
	}

}

$condition = true;

if($condition == true){
	echo "Yep this is true!";
} else {
	echo "No this is not true";
}

$filterGrade = 4;
echo "<br/>";

if( ($filterGrade > 3 && $filterGrade < 6) || $filterGrade == 1){
	echo "Should be worried";
} else {
	echo "It's fine";
}

$condition = false || true;
$condition = true && false;

$students = [
	[
		"name" => "Max1",
		"grade" => 3
	],
	[
		"name" => "Max2",
		"grade" => 2
	],
	[
		"name" => "Max3",
		"grade" => 5
	],
];
echo "<br/>";

foreach ($students as $student){
	foreach ($student as $key => $value){
		if($key == "grade" AND $value > 4){
			echo $student["name"]. " ". $student[$key]." is worse than 3";
		}
	}
}

echo "<br/>";

$filter = "gendr";

switch ($filter){
	case "gender":
		echo "Let's filter by gender";
		break;
	case "age":
		echo "Let's filter by age";
		break;
	case "course":
		echo "Let's filter by course";
		break;
	default:
		echo "unknown filter criteria";
}